# sabemos

_(Portuguese and Spanish: “we know”)_

A cloud-native self-hosting-friendly Q&A application

## Declaration of Intent

Build a web application using the following techniques:

- Python WSGI
- OIDC authentication
- RDBMS backend
- Container build

Features:

- Few application roles: admin, moderator, user
- Questions and answers (Q&A) are written and stored in a [GitHub](https://github.github.com/gfm/) or [GitLab](https://docs.gitlab.com/ee/user/markdown.html) flavored MarkDown subset
- Q&A can be tagged
- Answers can be voted for or against by all users
- One answer can be marked as the selected solution by the question poster
- Search over Q&A, questions only or answers only


## Contributions welcome

If you would like to contribute, please request project access using the link on the [overview page](https://gitlab.com/blackstream-x/sabemos).
